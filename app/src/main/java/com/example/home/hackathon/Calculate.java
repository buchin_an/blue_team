package com.example.home.hackathon;

public class Calculate {
    public float taxProfit(int sum, int procentRate, int month) {
        return sum * procentRate / month;
    }

    public float rate1(float oneMonthIncome, int procentRate) {
        return (oneMonthIncome/100)*procentRate;
    }
    public float everyMonthIcome(float rate, int deposite){
        return deposite - rate;
    }
    public double capitalisation( int sum, int procentRate, int month){
double power = Math.pow((1+procentRate),month);
        return sum*power;
    }

}
